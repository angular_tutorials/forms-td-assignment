import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isSubmitted = false;

  userData = {
    email: '',
    subscription: ''
  };

  onSubmit(form: NgForm) {
    this.isSubmitted = true;
    this.userData.email = form.value.email;
    this.userData.subscription = form.value.subscription;
    if (console.log(form)) {
      form.reset();
    }
  }
}
